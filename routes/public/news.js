var express = require('express');
var router = express.Router();

// MIDDLEWARES
// ==============================================
router.use(function(req, res, next) {
	console.log("ROUTER NEWS "+ req.method, req.url);
	next();	
});

// ROUTES
// ==============================================
router.get('/', function(req, res) {
	console.log("PUBLIC NEWS");
	res.send('I\'m the news page!');	
});

router.use('/news', router);

module.exports = router;