var express = require('express');
var router = express.Router();
var routerNews = require('./news');

// MIDDLEWARES
// ==============================================
router.use(function(req, res, next) {
	console.log("ROUTER "+ req.method, req.url);
	next();	
});

// ROUTES
// ==============================================
router.get('/', function(req, res) {
	console.log("PUBLIC HOME");
	res.send('I\'m the home page!');	
});

router.route('/about')
	.get(function(req, res, next) {
		console.log("PUBLIC ABOUT");
		res.send('I\'m the about page!');	
	}).post(function(req, res){
		res.statusCode = 403;
		var e = new Error('Forbidden');
	    next(e);
	});

router.use('/news', routerNews);

module.exports = router;