var express = require('express');
var router = express.Router();

// MIDDLEWARES
// ==============================================
router.use(function(req, res, next) {
	console.log("ROUTER PRIVATE "+ req.method, req.url);
	next();	
});

router.param('name', function(req, res, next, name) {
	console.log('ROUTER PRIVATE NAME - doing name validations on ' + name);	
	req.name = name.toUpperCase();	
	next();	
});

// ROUTES
// ==============================================
router.get('/', function(req, res) {
	console.log("PRIVATE HOME");
	res.send('I\'m the PRIVATE home page!');	
});

router.get('/hello/:name', function(req, res) {
	console.log("PRIVATE NAME");
	//res.send('hello ' + req.params.name + '!');
	res.send('hello ' + req.name + '!');
});

module.exports = router;
