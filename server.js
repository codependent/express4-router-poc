// server.js

// BASE SETUP
// ==============================================

var express = require('express');
var app     = express();
var port    = process.env.PORT || 8080;


// MIDDLEWARES
// ==============================================
app.use(function(req, res, next){
	console.log("GLOBAL MIDDLEWARE");
	next();
});

// ROUTES
// ==============================================

// sample route with a route the way we're used to seeing it
app.get('/', function(req, res) {
	res.redirect('/app');	
});

app.get('/sample', function(req, res) {
	res.send('this is a sample!');	
});

// we'll create our routes here

var routerPrivate = express.Router();

// route middleware that will happen on every request
routerPrivate.use(function(req, res, next) {
	// log each request to the console
	console.log("ROUTER PRIVATE "+ req.method, req.url);
	// continue doing what we were doing and go to the route
	next();	
});

// route middleware to validate :name
routerPrivate.param('name', function(req, res, next, name) {
	// do validation on name here
	// blah blah validation
	// log something so we know its working
	console.log('ROUTER PRIVATE NAME - doing name validations on ' + name);

	// once validation is done save the new item in the req
	req.name = name.toUpperCase();
	// go to the next thing
	next();	
});

routerPrivate.get('/', function(req, res) {
	console.log("PRIVATE HOME");
	res.send('I\'m the PRIVATE home page!');	
});

// route with parameters (http://localhost:8080/hello/:name)
routerPrivate.get('/hello/:name', function(req, res) {
	//res.send('hello ' + req.params.name + '!');
	res.send('hello ' + req.name + '!');
});

app.use('/app/private', routerPrivate);


// get an instance of router
var router = express.Router();

// route middleware that will happen on every request
router.use(function(req, res, next) {
	// log each request to the console
	console.log("ROUTER "+ req.method, req.url);
	// continue doing what we were doing and go to the route
	next();	
});

// home page route (/)
router.get('/', function(req, res) {
	console.log("PUBLIC HOME");
	res.send('I\'m the home page!');	
});

// about page route (/about)
router.route('/about')
	.get(function(req, res, next) {
		res.send('I\'m the about page!');	
	}).post(function(req, res){
		res.statusCode = 403;
		var e = new Error('Forbidden');
	    next(e);
	});

// apply the public routes to our application
app.use('/app', router);


// get an instance of router
var routerNews = express.Router();

// route middleware that will happen on every request
routerNews.use(function(req, res, next) {
	// log each request to the console
	console.log("ROUTER NEWS "+ req.method, req.url);
	// continue doing what we were doing and go to the route
	next();	
});

// home page route (/)
routerNews.get('/', function(req, res) {
	console.log("PUBLIC NEWS");
	res.send('I\'m the news page!');	
});

// apply the public news routes to our application
router.use('/news', routerNews);


app.route('/login')
	// show the form (GET http://localhost:8080/login)
	.get(function(req, res) {
		res.send('this is the login form');
	})

	// process the form (POST http://localhost:8080/login)
	.post(function(req, res) {
		console.log('processing');
		res.send('processing the login form!');
	}
);



// START THE SERVER
// ==============================================
app.listen(port, function(){
	console.log('Magic happens on port ' + port);
});
