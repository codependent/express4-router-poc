// server.js

// BASE SETUP
// ==============================================

var express = require('express');
var app     = express();
var port    = process.env.PORT || 8080;
var routesPublic = require('./routes/public');
var routesPrivate = require('./routes/private');


// MIDDLEWARES
// ==============================================
app.use(function(req, res, next){
	console.log("GLOBAL MIDDLEWARE");
	next();
});

// ROUTES
// ==============================================

// sample route with a route the way we're used to seeing it
app.get('/', function(req, res) {
	res.redirect('/app');	
});

app.get('/sample', function(req, res) {
	res.send('this is a sample!');	
});

app.use('/app/private', routesPrivate);
app.use('/app', routesPublic);

app.route('/login')
	// show the form (/login)
	.get(function(req, res) {
		res.send('this is the login form');
	})

	// process the form (POST /login)
	.post(function(req, res) {
		console.log('processing');
		res.send('processing the login form!');
	}
);

// START THE SERVER
// ==============================================
app.listen(port);
console.log('Magic happens on port ' + port);